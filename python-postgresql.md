Title:  PostgreSQL for Python (Ubuntu)
Author: Carlos A. Ospina
# PostgreSQL installing

```
sudo apt install postgresql postgresql-client postgresql-contrib
sudo apt install libpq-dev
sudo -u postgres psql postgres
```
#### Configure password
```
postgres=# \password postgres
<enter new password>

postgres=# \q
```
#### Install pgAdmin III
```
sudo apt install pgadmin3
```
---

# For python
```
sudo apt -y install python3-pip python3-dev build-essential
sudo pip3 install --upgrade pip
sudo pip3 install psycopg2
```
To work in anaconda
```
conda install -c https://conda.anaconda.org/anaconda psycopg2
```

## Code example
Importing package
```python
import psycopg2 as pg
```
Connecting to database `example`
```python
conn = pg.connect(database="example", host="localhost", user="postgres", password="pw_example")
```
After each transaction do
```python
conn.commit()
```
Creating tables
```python
with conn.cursor() as c:
    c.execute("CREATE TABLE person (name text, id char(10) NOT NULL)")
    c.execute("CREATE TABLE student (code char(7) NOT NULL, career text) INHERITS (person)")
conn.commit()
```
Inserting records
```python
with conn.cursor() as c:
    c.execute("INSERT INTO person (name, id) VALUES ('pablo', '1000000001')")
    c.execute("INSERT INTO person VALUES ('paco', '1000000002')")
    c.execute("INSERT INTO student (name, id, code, career) VALUES ('lucio', '1000000003', '1000001', 'ASI')")
    c.execute("INSERT INTO student VALUES ('lucas', '1000000004', '1000002', 'ASI')")
conn.commit()
```
Showwing data (SELECT query)
```python
print("All persons")
with conn.cursor() as c:
    c.execute("SELECT name FROM person")
    for record in c:
        print(tuple(record))
    conn.commit()
    print("All students")
    c.execute("SELECT name FROM student")
    for record in c:
        print(tuple(record))
conn.commit()
```
Closing connection
```python
conn.close()
```
# End