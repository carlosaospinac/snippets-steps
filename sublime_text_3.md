# Sublime Text 3
## Download <a href="https://www.sublimetext.com/3", target="_blank">here<a>
```
sudo dpkg -i <filename>
```

---
## Licence key

```
—– BEGIN LICENSE —–
Ryan Clark
Single User License
EA7E-812479
2158A7DE B690A7A3 8EC04710 006A5EEB
34E77CA3 9C82C81F 0DB6371B 79704E6F
93F36655 B031503A 03257CCC 01B20F60
D304FA8D B1B4F0AF 8A76C7BA 0FA94D55
56D46BCE 5237A341 CD837F30 4D60772D
349B1179 A996F826 90CDB73C 24D41245
FD032C30 AD5E7241 4EAA66ED 167D91FB
55896B16 EA125C81 F550AF6B A6820916
—— END LICENSE ——
```
## Package control
### Menu: View > Show Console...
Ir a <a href="https://packagecontrol.io/installation" target="_blank">este link</a>

### Algunos pasos:
- <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd>
- Buscar: "Install Package" (Package Control: Install Package)... Entrar
- Buscar los apquetes de interés

### Paquetes importantes:
- Terminal
- Python 3
- Git
- GitGutter

## User settings
Configuración específica para Sublime Text: El la siguiente configuración se
establece, entre otras opciones, la de guardar el automáticamente al perder el foco
la ventana.

### Menu: Preferences > Settings-User
```sublime-settings
{
	"ignored_packages":
	[
		"Vintage"
	],
	"rulers": [ 79 ],
	"save_on_focus_lost": true,
	"translate_tabs_to_spaces": true
}
```
