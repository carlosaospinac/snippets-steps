# Anaconda 3
## Download package from <a href="https://www.continuum.io/downloads#_unix" target="_blank">here</a>
```
bash <package_path>
```
Add in the *~/.bashrc* file ```export PATH=$PATH:/home/$USER/anaconda3/bin/```

```
echo "export PATH=\$PATH:/home/\$USER/anaconda3/bin/" >> ~/.zshrc
```

**Note:** For Zsh (Oh My Zsh) shell, the filename is *~/.zshrc*

### PostgreSQL
```
conda install -c https://conda.anaconda.org/anaconda psycopg2
```
### Pygal
```
conda install -c activisiongamescience pygal=2.1.1 cairosvg=1.0.20; conda install lxml
```
---
### Matplotlib in default Python3
```
sudo pip3 install matplotlib; sudo apt-get -y install python3-pyqt5
```
