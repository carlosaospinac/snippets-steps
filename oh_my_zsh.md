# Install **Oh My Zsh** shell

## Ubuntu
```
sudo apt update && sudo apt install -y curl vim git zsh
curl -L http://install.ohmyz.sh | sh
sudo chsh -s $(which zsh) $(whoami)
```

## Fedora
```
sudo dnf install -y curl vim git zsh
curl -L http://install.ohmyz.sh | sh
sudo chsh
...
... ... [/bin/bash]: /usr/bin/zsh
```
